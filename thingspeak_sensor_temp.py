import os 
import time
import httplib
import urllib

sleep =15
key = '1I8A6VIEJAUZTN3N' # Thingspeak channel to update

os.system('modprobe w1-gpio')
os.system('modrobe w1-therm')

temp_sensor = '/sys/bus/w1/devices/28-03129779a05b/w1_slave'

def temp_raw():
        f = open (temp_sensor, 'r')
        lines = f.readlines()
        f.close()
        return lines

def read_temp():
        lines = temp_raw()
        while lines[0].strip()[-3:] != 'YES':
                time.sleep(0.2)
                lines = temp_raw()
        temp_output = lines[1].find('t=')
        if temp_output != -1:
                temp_string = lines[1].strip()[temp_output+2:]
                temp_c = float (temp_string) / 1000.0
                temp_f = temp_c * 9.0 / 5.0 + 32.0
                return temp_c
def thermometer():
    while True:
        #Calculate CPU temperature of Raspberry Pi in Degrees C
        temp = read_temp() # Get Raspberry Pi CPU temp
        params = urllib.urlencode({'field4': temp, 'key':key }) 
        headers = {"Content-type": "application/x-www-form-urlencoded","Accept": "text/plain"}
        conn = httplib.HTTPConnection("api.thingspeak.com:80")
        try:
            conn.request("POST", "/update", params, headers)
            response = conn.getresponse()
            print (temp)
            print (response.status, response.reason)
            data = response.read
            conn.close()
        except:
            print ("connection failed")
        break
#sleep for desired amount of time
if __name__ == "__main__":
        while True:
                thermometer()
                time.sleep(sleep)


