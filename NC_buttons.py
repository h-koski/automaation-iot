from time import sleep
import RPi.GPIO as GPIO

nappi1 = 12
nappi2 = 16

GPIO.setmode(GPIO.BOARD)
GPIO.setup(11,GPIO.OUT)
GPIO.setup(nappi1,GPIO.IN,pull_up_down=GPIO.PUD_UP)
GPIO.setup(nappi2,GPIO.IN,pull_up_down=GPIO.PUD_UP)

while True:
    if GPIO.input(nappi1) == 1:
        print("Nappia 1 painettu")
        GPIO.output(11,1)
        sleep(.1)
    if GPIO.input(nappi2) == 1:
        print("Nappia 2 painettu")
        GPIO.output(11,0)
        sleep(.1)
