#!/usr/bin/env python
# Modified by Manne Tervaskanto 16.11.2018
#import urllib2,json,time
# Version for Python 3.7. Need to replace urllib2 with urllib.request library.
import urllib.request,json,time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)
GPIO.setup(11,GPIO.OUT)

READ_API_KEY='RED34L5R8H4W70H0'
CHANNEL_ID=1132618
x=1     #loop counter, init =1
sleep=5.0 #seconds
#ScriptTimeStamp=time.time #testing only

try:

    while True:

        conn = urllib.request.urlopen("https://api.thingspeak.com/channels/%s/feeds/last.json?api_key=%s" \
                                   % (CHANNEL_ID,READ_API_KEY))
        response = conn.read()
        print ("http status code=%s" % (conn.getcode()))
        data=json.loads(response)
        TimefromDB=data['created_at']

        if x==1:
            ScriptTimeStamp=TimefromDB
            print ("Started polling new data in ThingSpeak DB from user",CHANNEL_ID)
            print ("Latest timestamp to compare",data['created_at'])
        if TimefromDB != ScriptTimeStamp and x>1:
            print ("Found new data in ThingSpeak DB")
            print (data['created_at'])
            tila = int(data['field7'])
            GPIO.output(11,tila)
            
            
        if TimefromDB == ScriptTimeStamp and x>1:
            print ("No new data in ThingSpeak DB")
            
        running_time=((x*sleep-sleep)/60.0)
        print ("Script running time",running_time, "minutes")
        print ("Sleeping",sleep,"seconds")
        ScriptTimeStamp=TimefromDB
        x=x+1
        conn.close()
        time.sleep(sleep)

finally:
    conn.close()
    print ("Connection lost")







